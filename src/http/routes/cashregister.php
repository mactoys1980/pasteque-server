<?php

use \Pasteque\Server\System\API\APICaller;
use \Pasteque\Server\System\API\APIResult;

/**
 * GET cashregisterGetAllGet
 * Summary:
 * Notes: Get a array of all CashRegister
 * Output-Formats: [application/json]
 */
$app->GET('/api/cashregister/getAll', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'cashRegister', 'getAll'));
});


/**
 * GET cashregisterGetbylabelLabelGet
 * Summary:
 * Notes: Get a cash register by this label
 * Output-Formats: [application/json]
 */
$app->GET('/api/cashregister/getByReference/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $apiRes = APICaller::run($ptApp, 'cashRegister', 'getByReference',
            $args['reference']);
    if ($apiRes->getStatus() == APIResult::STATUS_CALL_OK
            && $apiRes->getContent() === null) {
        return $response->withStatus(404, 'Cash register not found');
    }
    return $response->withApiResult($apiRes);
});

$app->GET('/api/cashregister/getByName/{name}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $apiRes = APICaller::run($ptApp, 'cashRegister', 'getByName',
            $args['name']);
    if ($apiRes->getStatus() == APIResult::STATUS_CALL_OK
            && $apiRes->getContent() === null) {
        return $response->withStatus(404, 'Cash register not found');
    }
    return $response->withApiResult($apiRes);
});

/**
 * GET cashregisterIdGet
 * Summary:
 * Notes: Get a CashRegiter
 * Output-Formats: [application/json]
 */
$app->GET('/api/cashregister/{id}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'cashRegister', 'get', $args['id']));
});

/** Low level call. If an id is set, it's an update. If not, it's a create. */
$app->POST('/api/cashregister', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    $cr = \Pasteque\Server\Model\CashRegister::fromStruct($tab, $ptApp->getDao());
    return $response->withApiResult(APICaller::run($ptApp, 'cashregister', 'write', $cr));
});

/** Create a new cash register from it's reference. The reference is read from url
 * and ignored from data.
 * Returns an error if an id is given or if a cash register already exists
 * with the given reference. */
$app->PUT('/api/cashregister/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'New record cannot have an Id');
    }
    $tab['reference'] = $args['reference'];
    $cr = \Pasteque\Server\Model\CashRegister::fromStruct($tab, $ptApp->getDao());
    // Check for an existing reference
    $existingCrReq = APICaller::run($ptApp, 'cashregister', 'getByReference', $cr->getReference());
    if ($existingCrReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingCrReq);
    }
    if ($existingCrReq->getContent() != null) {
        return $response->withStatus(400, 'Reference is already taken');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'cashregister', 'write', $cr));
});
