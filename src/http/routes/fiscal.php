<?php

use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;
use \Pasteque\Server\Model\FiscalTicket;
use \Pasteque\Server\System\DateUtils;
use \Pasteque\Server\System\Login;
use \Pasteque\Server\System\API\APICaller;
use \Pasteque\Server\System\API\APIResult;
use \Pasteque\Server\System\DAO\DAOCondition;

function fiscalLogin($app, $ptApp, $request, $response) {
    $data = $request->getParsedBody();
    if (!empty($data['user']) && !empty($data['password'])) {
        $apiResult = APICaller::run($ptApp, 'login', 'login',
                ['login' => $data['user'], 'password' => $data['password']]);
        if ($apiResult->getStatus() == APIResult::STATUS_CALL_OK) {
            // Set the cookie
            $newToken = $apiResult->getContent();
            if ($newToken != null) {
                $response = $response->withHeader(Login::TOKEN_HEADER, $newToken);
                $cookie = SetCookie::create(Login::TOKEN_HEADER)
                        ->withValue($newToken)
                        ->withMaxAge($ptApp->getJwtTimeout())
                        ->withPath($request->getUri()->getPath())
                        ->withDomain($request->getUri()->getHost());
                $response = FigResponseCookies::set($response, $cookie);
                $user = $ptApp->getIdentModule()->getUser($data['user']);
                $ptApp->login($user);
            }
        }
    }
    return $response;
}
function fiscalTpl($ptApp, $response, $template, $data = []) {
    $body = $response->getBody();
    $body->write(file_get_contents(__DIR__ . '/../templates/header.html'));
    require_once(__DIR__ . '/../templates/' . $template);
    $content = render($ptApp, $data);
    $body->write($content);
    $body->write(file_get_contents(__DIR__ . '/../templates/footer.html'));
}

$app->any('/fiscal/z/{sequence}', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $response = fiscalLogin($this, $ptApp, $request, $response);
    if ($ptApp->getCurrentUser() == null) {
        fiscalTpl($ptApp, $response, 'login.php');
        return $response;
    } else {
        fiscalTpl($ptApp, $response, 'z.html');
        return $response;
    }
});

/** Z ticket listing */
$app->any('/fiscal/sequence/{sequence}/z/', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $response = fiscalLogin($this, $ptApp, $request, $response);
    if ($ptApp->getCurrentUser() == null) {
        fiscalTpl($ptApp, $response, 'login.php');
        return $response;
    } else {
        // Get pagination input
        $queryParams = $request->getQueryParams();
        $page = (!empty($queryParams['page'])) ? $queryParams['page'] : 0;
        $sequence = $args['sequence'];
        $count = 10;
        // Get total count of Z tickets (for pagination)
        $apiCount = APICaller::run($ptApp, 'fiscal', 'countZ',
                ['sequence' => $sequence]);
        $pageCount = 0;
        if ($apiCount->getStatus() != APIResult::STATUS_CALL_OK) {
            fiscalTpl($ptApp, $response, 'apierror.php', $apiCount);
            return $response;
        } else {
            $zCount = $apiCount->getContent();
            $pageCount = intval($apiCount->getContent() / $count);
            if ($zCount % $count > 0) { $pageCount++; }
        }
        // Get Z tickets
        $apiResult = APICaller::run($ptApp, 'fiscal', 'listZ',
                ['sequence' => $sequence, 'count' => $count, 'page' => $page]);
        if ($apiResult->getStatus() == APIResult::STATUS_CALL_OK) {
            // Convert tickets to structs and check signature
            $data = $apiResult->getContent();
            $structs = [];
            if ($page == 0 && count($data) > 0) {
                $struct = $data[0]->toStruct();
                $struct['signature'] = $data[0]->checkSignature(null) ? 'ok' : 'NOK';
                $structs[] = $struct;
            }
            for ($i = 1; $i < count($data); $i++) {
                $struct = $data[$i]->toStruct();
                $struct['signature'] = $data[$i]->checkSignature($data[$i - 1]) ? 'ok' : 'NOK';
                $structs[] = $struct;
            }
            // Render
            fiscalTpl($ptApp, $response, 'listz.php', ['sequence' => $sequence,
                            'page' => $page, 'pageCount' => $pageCount,
                            'z' => $structs]);
        } else {
            fiscalTpl($ptApp, $response, 'apierror.php', $apiResult);
        }
        return $response;
    }
});

/** Tickets listing */
$app->any('/fiscal/sequence/{sequence}/tickets/', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $response = fiscalLogin($this, $ptApp, $request, $response);
    if ($ptApp->getCurrentUser() == null) {
        fiscalTpl($ptApp, $response, 'login.php');
        return $response;
    } else {
        // Get pagination input
        $queryParams = $request->getQueryParams();
        $page = (!empty($queryParams['page'])) ? $queryParams['page'] : 0;
        $sequence = $args['sequence'];
        $count = 10;
        // Get total count of tickets (for pagination)
        $apiCount = APICaller::run($ptApp, 'fiscal', 'countTickets',
                ['sequence' => $sequence]);
        $pageCount = 0;
        if ($apiCount->getStatus() != APIResult::STATUS_CALL_OK) {
            fiscalTpl($ptApp, $response, 'apierror.php', $apiCount);
            return $response;
        } else {
            $zCount = $apiCount->getContent();
            $pageCount = intval($apiCount->getContent() / $count);
            if ($zCount % $count > 0) { $pageCount++; }
        }
        // Get tickets
        $apiResult = APICaller::run($ptApp, 'fiscal', 'listTickets',
                ['sequence' => $sequence, 'count' => $count, 'page' => $page]);
        if ($apiResult->getStatus() == APIResult::STATUS_CALL_OK) {
            // Convert tickets to structs and check signature
            $data = $apiResult->getContent();
            $structs = [];
            if ($page == 0 && count($data) > 0) {
                $struct = $data[0]->toStruct();
                $struct['signature'] = $data[0]->checkSignature(null) ? 'ok' : 'NOK';
                $structs[] = $struct;
            }
            for ($i = 1; $i < count($data); $i++) {
                $struct = $data[$i]->toStruct();
                $struct['signature'] = $data[$i]->checkSignature($data[$i - 1]) ? 'ok' : 'NOK';
                $structs[] = $struct;
            }
            // Render
            fiscalTpl($ptApp, $response, 'listtkts.php', ['sequence' => $sequence,
                            'page' => $page, 'pageCount' => $pageCount,
                            'tickets' => $structs]);
        } else {
            fiscalTpl($ptApp, $response, 'apierror.php', $apiResult);
        }
        return $response;
    }
});

/** Fiscal home page */
$app->any('/fiscal/', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $response = fiscalLogin($this, $ptApp, $request, $response);
    if ($ptApp->getCurrentUser() == null) {
        fiscalTpl($ptApp, $response, 'login.php');
        return $response;
    } else {
        $apiResult = APICaller::run($ptApp, 'fiscal', 'getSequences');
        if ($apiResult->getStatus() == APIResult::STATUS_CALL_OK) {
            $data = $apiResult->getContent();
            fiscalTpl($ptApp, $response, 'menu.php', $data);
        } else {
            fiscalTpl($ptApp, $response, 'apierror.php', $apiResult);
        }
        return $response;
    }
});

/** Fiscal export */
$app->GET('/fiscal/export', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $response = fiscalLogin($this, $ptApp, $request, $response);
    if ($ptApp->getCurrentUser() == null) {
        fiscalTpl($ptApp, $response, 'login.php');
        return $response;
    } else {
        $params = $request->getQueryParams();
        $interval = null;
        $from = null;
        $to = new DateTime();
        if (!empty($params['period'])) {
            try {
                $interval = new DateInterval($params['period']);
                $from = clone $to;
                $from->sub($interval);
            } catch (Exception $e) {
                return $response->withStatus(400, 'Bad Request');
            }
        } elseif (!empty($params['from'])) {
            $from = DateUtils::readDate($params['from']);
            if ($from === false) {
                return $response->withStatus(400, 'Bad Request');
            }
            if (!empty($params['to'])) {
                $to = DateUtils::readDate($params['to']);
                if ($to === false) {
                    return $response->withStatus(400, 'Bad Request');
                }
            }
        }
        $jsonFileName = tempnam(sys_get_temp_dir(), 'pasteque_fiscal_export_');
        $zipFileName = tempnam(sys_get_temp_dir(), 'pasteque_fiscal_export_');
        try {
            // Create tmp file output
            $exportName = '';
            if ($from !== null) {
                $exportName = sprintf('fiscal_export-%s-%s', $from->format('Ymd_Hi'), $to->format('Ymd_Hi'));
            } else {
                $exportName = sprintf('fiscal_export-%s', $to->format('Ymd_Hi'));
            }
            $file = fopen($jsonFileName, 'w');
            fwrite($file, '['); // Init json string
            // Get all fiscal tickets
            // Run by batches to limit memory consumption
            $batchSize = 100;
            $page = 0;
            $done = false;
            $found = false;
            $searchConds = null;
            if ($from !== null) {
                $searchConds = [new DAOCondition('date', '>=', $from),
                        new DAOCondition('date', '<=', $to)];
            }
            while (!$done) {
                $apiResult = APICaller::run($ptApp, 'fiscal', 'search', [$searchConds, $batchSize, $batchSize * $page, ['type', 'sequence', 'number']]);
                $page++;
                if ($apiResult->getStatus() == APIResult::STATUS_CALL_OK) {
                    // Convert tickets to struct
                    $data = $apiResult->getContent();
                    if (count($data) == 0) {
                        $done = true;
                    } else {
                        $found = true;
                        $ftkts = [];
                        for ($i = 0; $i < count($data); $i++) {
                            $ftkts[] = $data[$i]->toStruct();
                        }
                        $strData = json_encode($ftkts);
                        // Remove enclosing '[' and ']' before appending and add ',' for next the record
                        $strData = substr($strData, 1, -1) . ',';
                        fwrite($file, $strData);
                    }
                } else {
                    fclose($file);
                    unlink($jsonFileName);
                    unlink($zipFileName);
                    fiscalTpl($ptApp, $response, 'apierror.php', $apiResult);
                    return $response;
                }
            }
            // End json string and close file
            if ($found) {
                fseek($file, -1, SEEK_END);
            }
            fwrite($file, "]\n");
            fclose($file);
            // Compress the file
            $zip = new ZipArchive();
            $zip->open($zipFileName, ZipArchive::CREATE);
            $zip->addFile($jsonFileName, sprintf('%s.txt', $exportName));
            $zip->close();
            $response = $response->withHeader('content-type', 'application/zip');
            $response = $response->withHeader('content-disposition', sprintf('attachment; filename="%s.zip"', $exportName));
            $zipFile = fopen($zipFileName, 'r+');
            $body = $response->getBody();
            while (!feof($zipFile)) {
                $body->write(fread($zipFile, 20480));
            }
            unlink($jsonFileName);
            unlink($zipFileName);
            return $response;
        } catch (Exception $e) {
            // Clean temp files on error
            if (file_exists($jsonFileName)) {
                unlink($jsonFileName);
            }
            if (file_exists($zipFileName)) {
                unlink($zipFileName);
            }
            throw $e;
        }
    }
});

/** Fiscal export API */
$app->GET('/api/fiscal/export', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $apiResult = APICaller::run($ptApp, 'fiscal', 'getAll', [['type', 'sequence', 'number']]);
    return $response->withAPIResult($apiResult);
});

function fiscal_importTickets($ptApp, $data) {
    $tkts = [];
    foreach ($data as $tkt) {
        $tkt['date'] = DateUtils::readDate($tkt['date']);
        if (!empty($tkt['id'])) {
            // We don't care about id as FiscalTicketAPI checks with snapshots.
            // And it messes things up with Doctrine
            if ($tkt['id']['number'] === 0 && $tkt['number'] === 0) {
                // Unless it's EOS which has to be messed up
                if ($ptApp->getDao()->read(FiscalTicket::class, $tkt['id']) == null) {
                    // only when it already exists.
                    unset($tkt['id']);
                }
            } else {
                unset($tkt['id']);
            }
        }
        $tkts[] = FiscalTicket::fromStruct($tkt, $ptApp->getDao());
    }
    $apiResult = APICaller::run($ptApp, 'fiscal', 'batchImport', [$tkts]);
    return $apiResult;
}

/** Fiscal import API */
$app->POST('/api/fiscal/import', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    if (!$ptApp->isFiscalMirror()) {
        return $response->withStatus(400, 'Only available for Fiscal Mirrors');
    }
    $data = $request->getParsedBody();
    return $response->withAPIResult(fiscal_importTickets($ptApp, $data));
});

/** Fiscal import interface */
$app->POST('/fiscal/import', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    if (!$ptApp->isFiscalMirror()) {
        return $response->withStatus(400, 'Only available for Fiscal Mirrors');
    }
    $files = $request->getUploadedFiles();
    if (empty($files['file'])) {
        return $response->withStatus(400, 'Bad Request');
    }
    $file = $files['file'];
    $stream = $file->getStream();
    $fileName = $stream->getMetadata('uri');
    // Try to unzip if required
    $zip = new ZipArchive();
    $res = $zip->open($fileName);
    if ($res === true) {
        if ($zip->count() != 1) {
            $apiResult = APIResult::reject("le fichier zip ne doit contenir qu'un seul fichier.");
            fiscalTpl($ptApp, $response, 'imported.php', $apiResult);
            return $response;
        }
        $data = $zip->getFromIndex(0);
        $zip->close();
    } else {
        $data = '';
        $read = $stream->read(2048);
        while ($read != '') {
            $data .= $read;
            $read = $stream->read(2048);
        }
    }
    $data = json_decode($data, true);
    if ($data === null) {
            $apiResult = APIResult::reject("les tickets fiscaux n'ont pu être lus depuis le fichier envoyé.");
            fiscalTpl($ptApp, $response, 'imported.php', $apiResult);
            return $response;
    }
    $apiResult = fiscal_importTickets($ptApp, $data);
    fiscalTPL($ptApp, $response, 'imported.php', $apiResult);
    return $response;
});
