<?php
use Pasteque\Server\System\API\APICaller;
use Pasteque\Server\System\API\APIResult;

/**
 * GET categoryGetAll
 * Summary:
 * Notes: Get an array of all Categories
 * Output-Formats: [application/json]
 */
$app->GET('/api/category/getAll', function($request, $response, $args) {

    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'category','getAll'));


});


/**
 * GET categoryGetChildrens
 * Summary:
 * Notes: Get an array of Categories from a parent Category Id
 * Output-Formats: [application/json]
 *  * @SWG\Get(
 *     path="/api/category/getChildrens",
 *     @SWG\Response(response="200", description="Get an array of Categories from a parent Category Id")
 * )
 */
$app->GET('/api/category/getChildrens', function($request, $response, $args) {

    $queryParams = $request->getQueryParams();
    $parentId = $queryParams['parentId'];
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'category','getChildren',[$parentId]));
});


/**
 * GET categoryId
 * Summary:
 * Notes: Get a Category
 * Output-Formats: [application/json]
 * @SWG\Get(
 *     path="/api/category/{id}",
 *     @SWG\Response(response="200", description="get a category")
 * )
 */
$app->GET('/api/category/{id}', function($request, $response, $args) {


    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'category','get',$args));

});




/**
 * @deprecated Use PUT with reference or POST instead.
 * PUT categoryCreateupdate
 * Summary:
 * Notes: create or modify a category
 * Output-Formats: [application/json]
 * @SWG\Put(
 *     path="/api/category",
 *     tags={"category"},
 *     operationId="updateCategory",
 *     summary="Update an existing category",
 *     description="",
 *     consumes={"application/json", "application/xml"},
 *     produces={"application/xml", "application/json"},
 *     @SWG\Parameter(
 *         name="body",
 *         in="body",
 *         description="category object that needs to be added",
 *         required=true,
 *         @SWG\Schema(ref="#/definitions/Category"),
 *     ),
 *     @SWG\Response(
 *         response=400,
 *         description="Invalid ID supplied",
 *     ),
 *     @SWG\Response(
 *         response=404,
 *         description="Category not found",
 *     ),
 *     @SWG\Response(
 *         response=405,
 *         description="Validation exception",
 *     ),
 *     security={{"pasteque_auth":{"write:categories", "read:categories"}}}
 * )
 */
$app->PUT('/api/category', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    $category = \Pasteque\Server\Model\Category::fromStruct($tab,$ptApp->getDao());
    return $response->withApiResult(APICaller::run($ptApp, 'category', 'write', $category));
});
/** Low level call. If an id is set, it's an update. If not, it's a create. */
$app->POST('/api/category', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    $category = \Pasteque\Server\Model\Category::fromStruct($tab,$ptApp->getDao());
    return $response->withApiResult(APICaller::run($ptApp, 'category', 'write', $category));
});
/** Create a new category from it's reference. The reference is read from url
 * and ignored from data.
 * Returns an error if an id is given or if a category already exists
 * with the given reference. */
$app->PUT('/api/category/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'New record cannot have an Id');
    }
    $tab['reference'] = $args['reference'];
    $category = \Pasteque\Server\Model\Category::fromStruct($tab,$ptApp->getDao());
    // Check for an existing reference
    $existingCatReq = APICaller::run($ptApp, 'category', 'getByReference', $category->getReference());
    if ($existingCatReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingCatReq);
    }
    if ($existingCatReq->getContent() != null) {
        return $response->withStatus(400, 'Reference is already taken');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'category', 'write', $category));
});
/** Update an existing category from it's reference. Returns an error if an id
 * is given or if there aren't any category with this reference. */
$app->PATCH('api/category/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'Do not send Id, use reference instead.');
    }
    $tab['reference'] = $args['reference'];
    $category = \Pasteque\Server\Model\Category::fromStruct($tab,$ptApp->getDao());
    // Check for an existing reference
    $existingCatReq = APICaller::run($ptApp, 'category', 'getByReference', $category->getReference());
    if ($existingCatReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingCatReq);
    }
    if ($existingCatReq->getContent() == null) {
        return $response->withStatus(404, 'No category found.');
    }
    if ($existingCatReq->getContent()->getId() != $category->getId()) {
        return $response->withStatus(500, 'Id mismatch.');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'category', 'write', $category));
});
