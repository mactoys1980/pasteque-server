<?php
use Pasteque\Server\System\API\APICaller;
use Pasteque\Server\System\API\APIResult;

/**
 * GET currencyGetAll
 * Summary:
 * Notes: Get an array of all Currencies
 * Output-Formats: [application/json]
 */
$app->GET('/api/currency/getAll', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'currency','getAll'));
});

/** Low level call. If an id is set, it's an update. If not, it's a create. */
$app->POST('/api/currency', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    $currency = \Pasteque\Server\Model\Currency::fromStruct($tab,$ptApp->getDao());
    return $response->withApiResult(APICaller::run($ptApp, 'currency', 'write', $currency));
});

/** Create a new currency from it's reference. The reference is read from url
 * and ignored from data.
 * Returns an error if an id is given or if a currency already exists
 * with the given reference. */
$app->PUT('/api/currency/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'New record cannot have an Id');
    }
    $tab['reference'] = $args['reference'];
    $currency = \Pasteque\Server\Model\Currency::fromStruct($tab,$ptApp->getDao());
    // Check for an existing reference
    $existingCurrReq = APICaller::run($ptApp, 'currency', 'getByReference', $currency->getReference());
    if ($existingCurrReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingCurrReq);
    }
    if ($existingCurrReq->getContent() != null) {
        return $response->withStatus(400, 'Reference is already taken');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'currency', 'write', $currency));
});

/** Update an existing currency from it's reference. Returns an error if an id
 * is given or if there aren't any category with this reference. */
$app->PATCH('api/currency/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'Do not send Id, use reference instead.');
    }
    $tab['reference'] = $args['reference'];
    $currency = \Pasteque\Server\Model\Currency::fromStruct($tab,$ptApp->getDao());
    // Check for an existing reference
    $existingCurrReq = APICaller::run($ptApp, 'currency', 'getByReference', $currency->getReference());
    if ($existingCurrReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingCurrReq);
    }
    if ($existingCurrReq->getContent() == null) {
        return $response->withStatus(404, 'No currency found.');
    }
    if ($existingCurrReq->getContent()->getId() != $currency->getId()) {
        return $response->withStatus(500, 'Id mismatch.');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'currency', 'write', $currency));
});
