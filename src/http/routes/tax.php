<?php

use \Pasteque\Server\System\API\APICaller;

/**
 * GET taxGetAllGet
 * Summary:
 * Notes: Get a array of all Tax
 * Output-Formats: [application/json]
 */
$app->GET('/api/tax/getAll', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'tax', 'getAll'));
});


/**
 * GET taxIdGet
 * Summary:
 * Notes: Get a Tax
 * Output-Formats: [application/json]
 */
$app->GET('/api/tax/{id}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'tax', 'get', $args['id']));
});

