<?php

use \Pasteque\Server\System\API\APICaller;
use \Pasteque\Server\System\API\APIResult;

/**
 * GET server version
 * Summary:
 * Notes:
 * Output-Formats: [application/json]
 */
$app->GET('/api/sync/{cashregister}', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp,
                    'sync', 'syncCashRegister', $args['cashregister']));
});

$app->GET('/api/sync', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp,
                    'sync', 'sync'));
});