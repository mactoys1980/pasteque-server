<?php

use \Pasteque\Server\Model\Customer;
use \Pasteque\Server\System\DateUtils;
use \Pasteque\Server\System\API\APICaller;
use \Pasteque\Server\System\API\APIResult;

/**
 * GET customerGetAllGet
 * Summary:
 * Notes: Get an array of all Customers
 * Output-Formats: [application/json]
 */
$app->GET('/api/customer/getAll', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'customer', 'getAll'));
});


/**
 * GET customerGetTopGet
 * Summary:
 * Notes: Get top (limit default 10) customer sorted by count of tickets
 * Output-Formats: [application/json]
 */
$app->GET('/api/customer/getTop', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'customer', 'getTop'));
});


/**
 * GET customerIdGet
 * Summary:
 * Notes: Get a Customer
 * Output-Formats: [application/json]
 */
$app->GET('/api/customer/{id}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'customer', 'get', $args['id']));
});

/** Create/update a customer without changing it's balance. */
$app->POST('/api/customer', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $structCust = $request->getParsedBody();
    if (!empty($structCust['customers'])) {
        // Only here for backward compatibility, see oldApiCustomer below.
        return oldApiCustomer($ptApp, $response, $structCust['customers']);
    }
    $structCust['expireDate'] = DateUtils::readDate($structCust['expireDate']);
    if ($structCust['expireDate'] === false) {
        return $response->withStatus(400, 'Invalid expireDate');
    }
    $customer = Customer::fromStruct($structCust, $ptApp->getDao());
    return $response->withApiResult(APICaller::run($ptApp, 'customer', 'write',
                    $customer));
});

/** @deprecated
 * This is the POST api/customer route with 'customers' as POST parameter.
 * It is there until it is removed from pasteque-android which uses it. */
function oldApiCustomer($ptApp, $response, $jsonCusts) {
    $structCusts = json_decode($jsonCusts, true);
    if ($structCusts === null) {
        return $response->withStatus(400, 'Unable to parse input data');
    }
    // Fill the array.
    $customers = [];
    foreach ($structCusts as $strC) {
        $customers[] = Customer::fromStruct($strC, $ptApp->getDao());
        // There is no expireDate in the form from Android, so ignore it.
    }
    return $response->withApiResult(APICaller::run($ptApp, 'customer', 'write',
                    $customers));
}

$app->PATCH('/api/customer/{id}/balance/{balance}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    if (!is_numeric($args['balance'])) {
        return $response->withStatus(400, 'Invalid balance');
    }
    $custId = $args['id'];
    $balance = floatval($args['balance']);
    $custApiResp = APICaller::run($ptApp, 'customer', 'get', $custId);
    if ($custApiResp->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withApiResult($custApiResp);
    }
    $balanceApiResp = APICaller::run($ptApp, 'customer', 'setBalance',
            [$custId, $balance]);
    if ($balanceApiResp->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withApiResult($balanceApiResp);
    }
    if ($balanceApiResp->getContent() === true) {
        return $response->withStatus(200);
    } else {
        return $response->withStatus(404, 'Customer not found');
    }
});