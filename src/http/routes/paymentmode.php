<?php

use \Pasteque\Server\System\API\APICaller;
use \Pasteque\Server\System\API\APIResult;

$app->GET('/api/paymentmode/getAll', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'paymentmode','getAll'));
});

$app->GET('/api/paymentmode/{id}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'paymentmode','get',$args));

});

/** Low level call. If an id is set, it's an update. If not, it's a create. */
$app->POST('/api/paymentmode', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    $pm = \Pasteque\Server\Model\PaymentMode::fromStruct($tab,$ptApp->getDao());
    return $response->withApiResult(APICaller::run($ptApp, 'paymentmode', 'write', $pm));
});

/** Create a new paymentMode from it's reference. The reference is read from url
 * and ignored from data.
 * Returns an error if an id is given or if a payment mode already exists
 * with the given reference. */
$app->PUT('/api/paymentmode/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'New record cannot have an Id');
    }
    $tab['reference'] = $args['reference'];
    $pm = \Pasteque\Server\Model\PaymentMode::fromStruct($tab, $ptApp->getDao());
    // Check for an existing reference
    $existingPMReq = APICaller::run($ptApp, 'paymentmode', 'getByReference', $pm->getReference());
    if ($existingPMReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingPMReq);
    }
    if ($existingPMReq->getContent() != null) {
        return $response->withStatus(400, 'Reference is already taken');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'paymentmode', 'write', $pm));
});

/** Update an existing paymentMode from it's reference. Returns an error if an id
 * is given or if there aren't any payment mode with this reference. */
$app->PATCH('api/paymentmode/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'Do not send Id, use reference instead.');
    }
    $tab['reference'] = $args['reference'];
    $pm = \Pasteque\Server\Model\PaymentMode::fromStruct($tab, $ptApp->getDao());
    // Check for an existing reference
    $existingPMReq = APICaller::run($ptApp, 'paymentmode', 'getByReference', $pm->getReference());
    if ($existingPMReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingPMReq);
    }
    if ($existingPMReq->getContent() == null) {
        return $response->withStatus(404, 'No payment mode found.');
    }
    if ($existingPMReq->getContent()->getId() != $pm->getId()) {
        return $response->withStatus(500, 'Id mismatch.');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'paymentmode', 'write', $pm));
});
