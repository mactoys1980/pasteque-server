<?php

use \Pasteque\Server\Exception\PastequeException;
use \Pasteque\Server\Model\Ticket;
use \Pasteque\Server\System\DateUtils;
use \Pasteque\Server\System\API\APICaller;
use \Pasteque\Server\System\API\APIResult;
use \Pasteque\Server\System\DAO\DAOCondition;

/** Get a single ticket. */
$app->GET('/api/ticket/{cashregister}/{number}', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $cashRes = APICaller::run($ptApp, 'cashRegister', 'get',
            $args['cashregister']);
    if ($cashRes->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withApiResult($cashRes);
    }
    $cashRegister = $cashRes->getContent();
    if ($cashRegister === null) {
        return $response->withStatus(404, 'Cash register not found');
    }
    $apiRes = APICaller::run($ptApp, 'ticket', 'search',
            [new DAOCondition('cashRegister', '=', $cashRegister),
                    new DAOCondition('number', '=', $args['number'])]);
    if ($apiRes->getStatus() == APIResult::STATUS_CALL_OK) {
        $ticket = $apiRes->getContent();
        if (count($ticket) > 0) {
            return $response->withAPIResult(APIResult::success($ticket[0]));
        } else {
            $response->withStatus(404, 'Ticket not found');
        }
    }
    return $response->withApiResult($apiRes);
});

$app->GET('/api/ticket/search', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $queryParams = $request->getQueryParams();
    // Get input
    $cashRegisterId = (empty($queryParams['cashRegister'])) ? null
            : $queryParams['cashRegister'];
    $dateStart = (empty($queryParams['dateStart'])) ? null
            : DateUtils::readDate(intval($queryParams['dateStart']));
    $dateStop =  (empty($queryParams['dateStop'])) ? null
            : DateUtils::readDate(intval($queryParams['dateStop']));
    $userId = (empty($queryParams['user'])) ? null
            : $queryParams['user'];
    $customerId = (empty($queryParams['customer'])) ? null
            : $queryParams['customer'];
    $offset = (empty($queryParams['offset'])) ? null
            : intval($queryParams['offset']);
    if ($offset === 0) {
        $offset = null;
    }
    $limit = (empty($queryParams['limit'])) ? null
            : intval($queryParams['limit']);
    if ($limit === 0) {
        $limit = null;
    }
    // Search criterias
    if ($dateStart === false) {
        return $response->withApiResult(APIResult::reject('Invalid dateStart'));
    }
    if ($dateStop === false) {
        return $response->withApiResult(APIResult::reject('Invalid dateStop'));
    }
    $cashRegister = null;
    if ($cashRegisterId !== null) {
        $cashRegRes = APICaller::run($ptApp, 'cashRegister', 'get', $cashRegisterId);
        if  ($cashRegRes->getStatus() != APIResult::STATUS_CALL_OK) {
            return $response->withApiResult($cashRegRes);
        }
        $cashRegister = $cashRegRes->getContent();
        if ($cashRegister === null) {
            return $response->withStatus(404, 'Cash register not found');
        }
    }
    $customer = null;
    if ($customerId !== null) {
        $custRes = APICaller::run($ptApp, 'customer', 'get', $customerId);
        if  ($custRes->getStatus() != APIResult::STATUS_CALL_OK) {
            return $response->withApiResult($custRes);
        }
        $customer = $custRes->getContent();
        if ($customer === null) {
            return $response->withStatus(404, 'Customer not found');
        }
    }
    $user = null;
    if ($userId !== null) {
        $userRes = APICaller::run($ptApp, 'user', 'get', $userId);
        if  ($userRes->getStatus() != APIResult::STATUS_CALL_OK) {
            return $response->withApiResult($userRes);
        }
        $user = $userRes->getContent();
        if ($user === null) {
            return $response->withStatus(404, 'User not found');
        }
    }
    $conditions = [];
    if ($cashRegister !== null) {
        $conditions[] = new DAOCondition('cashRegister', '=', $cashRegister);
    }
    if ($dateStart !== null) {
        $conditions[] = new DAOCondition('date', '>', $dateStart);
    }
    if ($dateStop !== null) {
        $conditions[] = new DAOCondition('date', '<', $dateStop);
    }
    if ($user !== null) {
        $conditions[] = new DAOCondition('user', '=', $user);
    }
    if ($customer !== null) {
        $conditions[] = new DAOCondition('customer', '=', $customer);
    }
    if (!empty($queryParams['count'])) {
        $tktRes = APICaller::run($ptApp, 'ticket', 'count', [$conditions]);
    } else {
        $tktRes = APICaller::run($ptApp, 'ticket', 'search',
                [$conditions, $limit, $offset]);
    }
    return $response->withApiResult($tktRes);
});

/** Get tickets from a session. */
$app->GET('/api/ticket/session/{cashregister}/{sequence}', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $cashRes = APICaller::run($ptApp, 'cashSession', 'get',
            [['cashRegister' => $args['cashregister'],
              'sequence' => $args['sequence']]]);
    if ($cashRes->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withApiResult($cashRes);
    }
    $session = $cashRes->getContent();
    if ($session === null) {
        return $response->withStatus(404, 'Cash session not found');
    }
    $apiRes = APICaller::run($ptApp, 'ticket', 'search',
            [[new DAOCondition('cashRegister', '=', $session->getCashRegister()),
             new DAOCondition('sequence', '=', $session->getSequence())]]);
    return $response->withApiResult($apiRes);
});

/**
 * POST ticketPost
 * Summary:
 * Notes: add a ticket
 * Output-Formats: [application/json]
 */
$app->POST('/api/ticket', function ($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $data = $request->getParsedBody();
    $jsonTkts = $data['tickets'];
    $structTkts = json_decode($jsonTkts, true);
    if ($structTkts === null) {
        return $response->withStatus(400, 'Unable to parse input data');
    }
    $result = ['successes' => [], 'failures' => [], 'errors' => []];
    foreach ($structTkts as $structTkt) {
        try {
            $structTkt['date'] = DateUtils::readDate($structTkt['date']);
            $ticket = Ticket::fromStruct($structTkt, $ptApp->getDao());
            $apiRes = APICaller::run($ptApp, 'ticket', 'write', $ticket);
            $tktResp = ['cashRegister' => $structTkt['cashRegister'],
                    'sequence' => $structTkt['sequence'],
                    'number' => $structTkt['number']];
            switch ($apiRes->getStatus()) {
            case APIResult::STATUS_CALL_OK:
                $result['successes'][] = $tktResp;
                break;
                // TODO: on ticket save failure, the signature of the next tickets are wrong.
                // Storing the ticket as a failure and add corrections would help.
            case APIResult::STATUS_CALL_REJECTED:
                if (is_a($apiRes->getContent(), PastequeException::class)) {
                    $tktResp['message'] = json_encode($apiRes->getContent()->toStruct());
                } else {
                    $tktResp['message'] = $apiRes->getContent();
                }
                $result['failures'][] = $tktResp;
                break;
            case APIResult::STATUS_CALL_ERROR:
            default:
                $tktResp['message'] = $apiRes->getContent();
                $result['errors'][] = $tktResp;
            }
        } catch (\UnexpectedValueException $e) {
            $tktResp = ['cashRegister' => (empty($structTkt['cashRegister'])) ?
                    null : $structTkt['cashRegister'],
                    'sequence' => (empty($structTkt['sequence'])) ?
                    null : $structTkt['sequence'],
                    'number' => (empty($structTkt['number'])) ?
                    null : $structTkt['number'],
                    'message' => $e->getMessage()];
            $result['failures'][] = $tktResp;
        }
    }
    // Always return status 200 at this point
    // to let the clients parse the content and see what was ok/wrong.
    return $response->withJson($result);
});
