<?php

use \Pasteque\Server\Model\TariffArea;
use \Pasteque\Server\System\API\APICaller;
use \Pasteque\Server\System\API\APIResult;

/**
 * GET tariffareaGetAllGet
 * Summary:
 * Notes: Get a array of all TariffArea
 * Output-Formats: [application/json]
 */
$app->GET('/api/tariffarea/getAll', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'tariffArea', 'getAll'));
});


/**
 * GET tariffareaIdGet
 * Summary:
 * Notes: Get a TariffArea
 * Output-Formats: [application/json]
 */
$app->GET('/api/tariffarea/{id}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'tariffArea', 'get', $args['id']));
});

/** Low level call. If an id is set, it's an update. If not, it's a create. */
$app->POST('/api/tariffarea', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    $ta = \Pasteque\Server\Model\TariffArea::fromStruct($tab, $ptApp->getDao());
    return $response->withApiResult(APICaller::run($ptApp, 'tariffArea', 'write', $ta));
});

/** Create a new tariff area from it's reference. Returns an error if an id is given
 * or if an area already exists with the given reference. */
$app->PUT('/api/tariffarea/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'New record cannot have an Id');
    }
    $tab['reference'] = $args['reference'];
    $ta = \Pasteque\Server\Model\TariffArea::fromStruct($tab, $ptApp->getDao());
    // Check for an existing reference
    $existingTAReq = APICaller::run($ptApp, 'tariffArea', 'getByReference',
            $ta->getReference());
    if ($existingTAReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingTAReq);
    }
    if ($existingTAReq->getContent() != null) {
        return $response->withStatus(400, 'Reference is already taken');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'tariffArea', 'write',
                    $ta));
});

/** Delete an existing tariff area from it's reference. Returns an error if an id
 * is given or if there aren't any area with this reference. */
$app->DELETE('/api/tariffarea/{reference}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    if (!empty($tab['id'])) {
        return $response->withStatus(400, 'Do not send Id, use reference instead.');
    }
    $tab['reference'] = $args['reference'];
    $ta = \Pasteque\Server\Model\TariffArea::fromStruct($tab, $ptApp->getDao());
    // Check for an existing reference
    $existingTAReq = APICaller::run($ptApp, 'tariffArea', 'getByReference',
            $ta->getReference());
    if ($existingTAReq->getStatus() != APIResult::STATUS_CALL_OK) {
        return $response->withAPIResult($existingTAReq);
    }
    if ($existingTAReq->getContent() == null) {
        return $response->withStatus(404, 'No tariff area found.');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'tariffArea', 'delete',
            $existingTAReq->getContent()));
});
