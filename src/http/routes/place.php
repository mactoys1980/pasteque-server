<?php

use \Pasteque\Server\Model\Floor;
use \Pasteque\Server\System\API\APICaller;
use \Pasteque\Server\System\API\APIResult;

$app->POST('/api/places', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $structFloors = $request->getParsedBody();
    if ($structFloors === null) {
        return $response->withStatus(400, 'Unable to parse input data');
    }
    $floors = [];
    foreach ($structFloors as $jsFloor) {
        $floor = Floor::fromStruct($jsFloor, $ptApp->getDao());
        $floors[] = $floor;
    }
    return $response->withApiResult(APICaller::run($ptApp, 'place', 'write', [$floors]));
});
