<?php

use \Pasteque\Server\Model\Resource;
use \Pasteque\Server\System\API\APICaller;

/**
 * GET roleNameGet
 * Summary:
 * Notes: Get a Resource by its label
 * Output-Formats: [application/json]
 */
$app->GET('/api/resource/{label}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    return $response->withApiResult(APICaller::run($ptApp, 'resource', 'get', $args['label']));
});

/** Insert or update a resource. */
$app->POST('/api/resource', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $tab = $request->getParsedBody();
    $res = \Pasteque\Server\Model\Resource::fromStruct($tab, $ptApp->getDao());
    return $response->withApiResult(APICaller::run($ptApp, 'resource', 'write', $res));
});

$app->DELETE('/api/resource/{label}', function($request, $response, $args) {
    $ptApp = $this->get('settings')['ptApp'];
    $label = $args['label'];
    if ($label != 'Printer.Ticket.Logo' && $label != 'Printer.Ticket.Header'
            && $label != 'Printer.Ticket.Footer' && $label != 'MobilePrinter.Ticket.Logo'
            && $label != 'MobilePrinter.Ticket.Header' && $label != 'MobilePrinter.Ticket.Footer') {
        return $response->withStatus(400, 'Cannot delete this resource');
    }
    return $response->withApiResult(APICaller::run($ptApp, 'resource', 'delete', $label));
});
