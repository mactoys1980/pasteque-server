<?php

use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;
use Pasteque\Server\System\Login;

/** Middleware to reject all calls except login API without a valid token
 * and add a new token to the response header. */
$loginMiddleware = function($request, $response, $next) {

    $ptApp = $this->get('settings')['ptApp'];

    // Check for logged user
    $userId = null;
    $token = Login::getToken();
    if ($token != null) {
        $userId = Login::getLoggedUserId($token, $ptApp->getJwtSecret(), $ptApp->getJwtTimeout());
        if ($userId !== null) {
            $user = $ptApp->getIdentModule()->getUser($userId);
            $ptApp->login($user);
        }
    }

    $path = $request->getUri()->getPath();
    if (substr($path, 0, 1) == '/') {
        $path = substr($path, 1);
    }
    $mustLogin = ('fiscal/' !== $path && 'api/login' !== $path
            && !$request->isOptions());

    if ($userId === null && $mustLogin) {
        // Reject the call because not authenticated.
        $response = $response->withStatus(403, 'Not logged');
    } else {
        // Pass the call to the regular route
        $response = $next($request, $response);
        // Inject fresh token
        if ($userId !== null) {
            $newToken = Login::issueAppToken($ptApp);
            $response = $response->withHeader(Login::TOKEN_HEADER, $newToken);
            $cookie = SetCookie::create(Login::TOKEN_HEADER)
                ->withValue($newToken)
                ->withMaxAge($ptApp->getJwtTimeout())
                ->withPath($request->getUri()->getPath())
                ->withDomain($request->getUri()->getHost());
            $response = FigResponseCookies::set($response, $cookie);
        } else {
            // Case of login without prior token. See login route which sets the cookie.
            // Nothing to do here.
        }
    }
    return $response;
};