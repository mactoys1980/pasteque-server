<?php

use \Pasteque\Server\System\DateUtils;

function renderZ($z) {
    $data = json_decode($z['content']);
    $output = ($z['content']);
    if ($data !== null) {
        $output = json_encode($data, \JSON_PRETTY_PRINT);
    }
    $ret = '<div>';
    $ret .= '<h3>Numéro : ' . $z['number'] . '</h3>';
    $ret .= '<p>Signature : ' . $z['signature'] . '</p>' ;
    $ret .= '<pre>' . htmlspecialchars($output) . '</pre>';
    $ret .= '</div>';
    return $ret;
}

function renderPagination($data) {
    $ret = '<p>Page</p>';
    $ret .= '<p>';
    for ($i = 0; $i < $data['pageCount']; $i++) {
        if ($data['page'] == $i) {
            $ret .= $i . ' ';
        } else {
            $ret .= '<a href=".?page=' . htmlspecialchars($i) . '">' . htmlspecialchars($i) . '</a> ';
        }
    }
    $ret .= '</p>';
    return $ret;
}

function render($ptApp, $data) {
    $ret = '<h2>Liste des tickets Z</h2>';
    $ret .= renderPagination($data);
    foreach ($data['z'] as $z) {
        $ret .= renderZ($z);
    }
    $ret .= renderPagination($data);
    return $ret;
}
