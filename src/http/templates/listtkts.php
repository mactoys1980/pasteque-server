<?php

use \Pasteque\Server\System\DateUtils;

function renderTicket($tkt) {
    $data = json_decode($tkt['content']);
    $output = ($tkt['content']);
    if ($data !== null) {
        $output = json_encode($data, \JSON_PRETTY_PRINT);
    }
    $ret = '<div>';
    $ret .= '<h3>Numéro : ' . $tkt['number'] . '</h3>';
    $ret .= '<p>Signature : ' . $tkt['signature'] . '</p>' ;
    $ret .= '<pre>' . htmlspecialchars($output) . '</pre>';
    $ret .= '</div>';
    return $ret;
}

function renderPagination($data) {
    $ret = '<p>Page</p>';
    $ret .= '<p>';
    for ($i = 0; $i < $data['pageCount']; $i++) {
        if ($data['page'] == $i) {
            $ret .= $i . ' ';
        } else {
            $ret .= '<a href=".?page=' . htmlspecialchars($i) . '">' . htmlspecialchars($i) . '</a> ';
        }
    }
    $ret .= '</p>';
    return $ret;
}

function render($ptApp, $data) {
    $ret = '<h2>Liste des tickets</h2>';
    $ret .= renderPagination($data);
    foreach ($data['tickets'] as $tkt) {
        $ret .= renderTicket($tkt);
    }
    $ret .= renderPagination($data);
    return $ret;
}
