<?php

function render($ptApp, $data) {
    $ret = '<h1>Consultation des données fiscale</h1>';
    $ret .= '<h2>Lister tous les tickets par caisse</h2>';
    $ret .= '<ul>';
    foreach ($data as $sequence) {
        $ret .= '<li>' . htmlspecialchars($sequence) . ' : <a href="./sequence/' . htmlspecialchars($sequence) . '/z/" target="_blank">Tickets Z</a> <a href="./sequence/' . htmlspecialchars($sequence) . '/tickets/" target="_blank">Tickets</a></li>';
    }
    $ret .= '</ul>';
    $ret .= '<h2>Export des tickets</h2>';
    $ret .= '<ul>';
    $ret .= '<li><a href="./export?period=P7D">Exporter une semaine</a></li>';
    $ret .= '<li><a href="./export?period=P14D">Exporter deux semaines</a></li>';
    $ret .= '<li><a href="./export?period=P1M">Exporter un mois</a></li>';
    $ret .= '<li><form action="export" method="get">';
    $ret .= '<label for="period-from">Exporter depuis le </label><input type="date" name="from" id="period-from" placeholder="aaaa/mm/jj" required="true" /> <input type="submit" value="Exporter" />';
    $ret .= '</form></li>';
    $ret .= '<li><form action="export" method="get">';
    $ret .= '<label for="interval-from">Exporter depuis le </label><input type="date" name="from" id="interval-from" placeholder="aaaa-mm-jj" required="true" /> <label for="interval-to">au </label><input type="date" name="to" id="interval-to" placeholder="aaaa-mm-jj" required="true" /> <input type="submit" value="Exporter" />';
    $ret .= '</form></li>';
    $ret .= '</ul>';
    if ($ptApp->isFiscalMirror()) {
        $ret .= '<h2>Import des tickets</h2>';
        $ret .= '<p>Exportez les tickets depuis votre instance d\'usage pour en reverser une copie sur ce miroir. Les tickets déjà importés seront ignorés.</p>';
        $ret .= '<p><form method="POST" action="import" enctype="multipart/form-data">';
        $ret .= '<input type="file" name="file" required="true" /> <input type="submit" value="Envoyer" /></form></p>';
    }
    return $ret;
}
