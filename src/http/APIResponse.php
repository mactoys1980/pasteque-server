<?php

namespace Pasteque\HTTP;

use Slim\Http\Response;
use Pasteque\Server\System\API\APIResult;

class APIResponse extends Response
{
    public function withAPIResult($result) {
        $status = $result->getStatus();
        switch ($status) {
            case APIResult::STATUS_CALL_OK:
                return $this->withJson($result->getStructContent());
            case APIResult::STATUS_CALL_REJECTED:
                return $this->withStatus(400, $result->getContent());
            case APIResult::STATUS_CALL_ERROR:
                return $this->withStatus(500, $result->getContent());
        }
    }
}
