<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Server\Model;

use \Pasteque\Server\System\DAO\DoctrineModel;

/**
 * Class PaymentMode
 * Outside Doctrine, PaymentModeValues and PaymentModeReturns are included
 * directly in struct and not only the reference by their IDs.
 * That means those classes doesn't exists for toStruct
 * and fromStruct (it's just an array of data).
 * I.e. values will appear like
 * "values": [{"value": <float>, "image": <binary>}]
 * @package Pasteque
 * @SWG\Definition(type="object")
 * @Entity
 * @Table(name="paymentmodes")
 */
class PaymentMode extends DoctrineModel
{ public function __toString() { return $this->getReference(); }

    const TYPE_DEFAULT = 0;
    /** Requires a customer */
    const CUST_ASSIGNED = 1;
    /** Uses customer's debt (includes CUST_ASSIGNED) */
    const CUST_DEBT = 3; // 2 + PaymentMode::CUST_ASSIGNED
    /** Uses customer's prepaid (includes CUST_ASSIGNED) */
    const CUST_PREPAID = 5; // 4 + PaymentMode::CUST_ASSIGNED;

    protected static function getDirectFieldNames() {
        return ['id', 'reference', 'label', 'backLabel', 'type', 'visible', 'dispOrder', 'hasImage'];
    }
    protected static function getAssociationFields() {
        return [
                [
                 'name' => 'values',
                 'class' => '\Pasteque\Server\Model\PaymentModeValue',
                 'array' => true,
                 'embedded' => true
                 ],
                [
                 'name' => 'returns',
                 'class' => '\Pasteque\Server\Model\PaymentModeReturn',
                 'array' => true,
                 'embedded' => true
                 ]
                ];
    }

    public function __construct() {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
        $this->returns = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Internal ID of the payment mode for performance issues.
     * @var integer
     * @SWG\Property()
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;
    public function getId() { return $this->id; }

    /**
     * Code of the payment mode, user-friendly ID.
     * This was previously 'code'. Is is passed to toStruct and fromStruct
     * for compatibility.
     * @var string
     * @SWG\Property()
     * @Column(type="string", unique=true)
     */
    protected $reference;
    public function getReference() { return $this->reference; }
    public function setReference($ref) { $this->reference = $ref; }

    /**
     * Label of the payment mode
     * @var string
     * @SWG\Property()
     * @Column(type="string")
     */
    protected $label;
    public function getLabel() { return $this->label; }
    public function setLabel($label) { $this->label = $label; }

    /**
     * Label of the payment mode when used for returning.
     * @var string
     * @SWG\Property()
     * @Column(type="string")
     */
    protected $backLabel = '';
    public function getBackLabel() { return $this->backLabel; }
    public function setBackLabel($backLabel) { $this->backLabel = $backLabel; }

    /**
     * Type of the payment mode (see constants).
     * @var int Type
     * @SWG\Property(format="int32")
     * @Column(type="integer")
     */
    protected $type = self::TYPE_DEFAULT;
    public function getType() { return $this->type; }
    public function setType($type) { $this->type = $type; }

    public function usesDebt() {
        return ($this->type & static::CUST_DEBT) == static::CUST_DEBT;
    }
    public function usesPrepay() {
        return ($this->type & static::CUST_PREPAID) == static::CUST_PREPAID;
    }

    /**
     * @OneToMany(targetEntity="\Pasteque\Server\Model\PaymentModeValue", mappedBy="paymentMode", cascade={"all"})
     * @OrderBy({"value" = "DESC"})
     */
    // Warning: no orphan removal as it doesn't work with Mysql. This is handled in PaymentModeAPI.
    protected $values;
    public function getValues() { return $this->values; }
    public function setValues($values) {
        $this->clearValues();
        foreach ($values as $value) {
            $this->addValue($value);
        }
    }
    public function addValue($value) {
        $this->values->add($value);
        $value->setPaymentMode($this);
    }
    public function clearValues() {
        foreach ($this->values->getKeys() as $key) {
            $this->values->remove($key);
        }
    }
    public function removeValue($value) {
        $this->values->removeElement($value);
    }

    /**
     * @OneToMany(targetEntity="\Pasteque\Server\Model\PaymentModeReturn", mappedBy="paymentMode", cascade={"all"})
     * @OrderBy({"minAmount" = "DESC"})
     */
    // Warning: no orphan removal as it doesn't work with Mysql. This is handled in PaymentModeAPI.
    protected $returns;
    public function getReturns() { return $this->returns; }
    public function setReturns($returns) {
        $this->clearReturns();
        foreach ($returns as $return) {
            $this->addReturn($return);
        }
    }
    public function addReturn($return) {
        $this->returns->add($return);
        $return->setPaymentMode($this);
        if ($return->getReturnMode() === null) {
            $return->setReturnMode($this);
        }
    }
    public function clearReturns() {
        foreach ($this->returns->getKeys() as $key) {
            $this->returns->remove($key);
        }
    }
    public function removeReturn($return) {
        $this->returns->removeElement($return);
    }

    /**
     * True if an image can be found for this model.
     * @var bool
     * @SWG\Property()
     * @Column(type="boolean")
     */
    protected $hasImage = false;
    public function getHasImage() { return $this->hasImage; }
    public function hasImage() { return $this->getHasImage(); }
    public function setHasImage($hasImage) { $this->hasImage = $hasImage; }

    /**
     * Order of display of the payment mode
     * @var int order
     * @SWG\Property(format="int32")
     * @Column(type="integer", name="disp_order")
     */
    protected $dispOrder = 0;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    /**
     * Is the payment mode currently active (visible on cash registers) ?
     * If invisible, the payment will still be useable in returns, but not
     * as main payment.
     * @var bool
     * @SWG\Property()
     * @Column(type="boolean")
     */
    protected $visible = true;
    public function getVisible() { return $this->visible; }
    /** Alias for getVisible (the latter is required for Doctrine) */
    public function isVisible() { return $this->getVisible(); }
    public function setVisible($visible) { $this->visible = $visible; }

    public function toStruct() {
        $struct = parent::toStruct();
        // Set code for compatibility
        $struct['code'] = $this->getReference();
        return $struct;
    }

    public static function fromStruct($struct, $dao, $parent = null) {
        $model = parent::fromStruct($struct, $dao, $parent);
        // Set reference from code for compatibility
        if (!empty($struct['code']) && $model->getReference() === null) {
            $model->setReference($struct['code']);
        }
        return $model;
    }
}
