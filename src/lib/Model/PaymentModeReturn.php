<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Server\Model;

use \Pasteque\Server\System\DAO\DoctrineModel;

/**
 * Class PaymentModeReturn
 * This defines how payment with a higher value than requested are handled.
 * @package Pasteque
 * @SWG\Definition(type="object")
 * @Entity
 * @Table(name="paymentmodereturns")
 */
class PaymentModeReturn extends DoctrineModel // Embedded class
{
    protected static function getDirectFieldNames() {
        return ['minAmount'];
    }
    protected static function getAssociationFields() {
        return [
                [
                 'name' => 'paymentMode',
                 'class' => '\Pasteque\Server\Model\PaymentMode',
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'returnMode',
                 'class' => '\Pasteque\Server\Model\PaymentMode',
                 'null' => false
                 ]
                ];
    }

    public function getId() {
        if ($this->getPaymentMode() === null) {
            return ['paymentMode' => null,
                    'minAmount' => $this->getMinAmount()];
        } else {
            return ['paymentMode' => $this->getPaymentMode()->getId(),
                    'minAmount' =>$this->getMinAmount()];
        }
    }

    /**
     * @var integer
     * @SWG\Property()
     * @Id
     * @ManyToOne(targetEntity="\Pasteque\Server\Model\PaymentMode", inversedBy="returns")
     * @JoinColumn(name="paymentmode_id", referencedColumnName="id", nullable=false)
     */
    protected $paymentMode;
    public function getPaymentMode() { return $this->paymentMode; }
    /** Set the payment mode. For new PaymentMode when the id is not already set
     * it also set returnMode when null.
     * That means you can create a self referencing structure without knowing
     * the PaymentMode id (which doesn't exists on create)
     */
    public function setPaymentMode($paymentMode) {
        $this->paymentMode = $paymentMode;
        if ($this->getReturnMode() === null) {
            $this->setReturnMode($paymentMode);
        }
    }

    /**
     * @var float
     * @SWG\Property()
     * @Id
     * @Column(type="float")
     */
    protected $minAmount = 0.0;
    public function getMinAmount() { return $this->minAmount; }
    public function setMinAmount($minAmount) { $this->minAmount = $minAmount; }

    /**
     * Can be null when creating a new return for a new PaymentMode.
     * In that case, when assigning the return mode to a payment mode,
     * it will then be linked to it.
     * @var integer
     * @SWG\Property()
     * @ManyToOne(targetEntity="\Pasteque\Server\Model\PaymentMode")
     * @JoinColumn(name="returnmode_id", referencedColumnName="id", nullable=false)
     */
    protected $returnMode;
    public function getReturnMode() { return $this->returnMode; }
    public function setReturnMode($returnMode) { $this->returnMode = $returnMode; }

    public static function fromStruct($struct, $dao, $embedded = false) {
        if (!isset($struct['minAmount'])) { // Allow the value 0
            throw new \UnexpectedValueException('No minAmount set');
        }
        $model = null;
        // Look for an existing PaymentMode
        if (!empty($struct['paymentMode'])) {
            $id = array('paymentMode' => $struct['paymentMode'],
                    'minAmount' => $struct['minAmount']);
            $model = $dao->read(static::class, $id);
            // Don't fail on not found, it's a new return mode.
            if (empty($struct['returnMode'])) {
                // But fail if return mode is not set
                // (this is valid only for new payment modes which reference themselves)
                throw new \UnexpectedValueException('Return mode not set for an existing payment mode.');
            }
        }
        // Otherwise create a new one.
        if ($model === null) {
            $model = new PaymentModeReturn();
        }
        if (!empty($struct['returnMode'])) {
            $pm = $dao->read(PaymentMode::class, $struct['returnMode']);
            if ($pm === null) {
                throw new \UnexpectedValueException('Return mode not found.');
            }
            $model->setReturnMode($pm);
        }
        $model->setMinAmount($struct['minAmount']);
        // At this point paymentMode and returnMode may both be null.
        // When assigning the return to a PaymentMode, both will be linked
        // to the parent.
        return $model;
    }
}
