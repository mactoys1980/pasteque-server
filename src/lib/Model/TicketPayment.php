<?php
//    Pastèque API
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Server\Model;

use \Pasteque\Server\System\DAO\DAO;
use \Pasteque\Server\System\DAO\DAOCondition;
use \Pasteque\Server\System\DAO\DoctrineModel;

/**
 * Class TicketPayment
 * @package Pasteque
 * @SWG\Definition(type="object")
 * @Entity
 * @Table(name="ticketpayments")
 */
class TicketPayment extends DoctrineModel // Embedded class
{
    protected static function getDirectFieldNames() {
        return ['dispOrder', 'amount', 'currencyAmount'];
    }
    protected static function getAssociationFields() {
        return [
                [
                 'name' => 'ticket',
                 'class' => '\Pasteque\Server\Model\Ticket',
                 'null' => true // because embedded
                 ],
                [
                 'name' => 'paymentMode',
                 'class' => '\Pasteque\Server\Model\PaymentMode'
                 ],
                [ // This array index (2) is hardcoded in fromStruct
                 'name' => 'currency',
                 'class' => '\Pasteque\Server\Model\Currency'
                 ]
                ];
    }
    public function getId() {
        if ($this->getTicket() === null) {
            return ['ticket' => null, 'dispOrder' => $this->getDispOrder()];
        } else {
            return ['ticket' => $this->getTicket()->getId(),
                'dispOrder' => $this->getDispOrder()];
        }
    }

    /**
     * @var integer
     * @SWG\Property
     * @ManyToOne(targetEntity="\Pasteque\Server\Model\Ticket", inversedBy="lines")
     * @JoinColumn(name="ticket_id", referencedColumnName="id", nullable=false)
     * @Id
     */
    protected $ticket;
    public function getTicket() { return $this->ticket; }
    public function setTicket($ticket) { $this->ticket = $ticket; }

    /**
     * Display order or number of the line
     * @var integer
     * @SWG\Property()
     * @Column(type="integer")
     * @Id
     */
    protected $dispOrder;
    public function getDispOrder() { return $this->dispOrder; }
    public function setDispOrder($dispOrder) { $this->dispOrder = $dispOrder; }

    /**
     * Type of the Payment
     * @var string
     * @SWG\Property()
     * @ManyToOne(targetEntity="\Pasteque\Server\Model\PaymentMode")
     * @JoinColumn(name="paymentmode_id", referencedColumnName="id", nullable=false)
     */
    protected $paymentMode;
    public function getPaymentMode() { return $this->paymentMode; }
    public function setPaymentMode($paymentMode) { $this->paymentMode = $paymentMode; }

    /**
     * Amount of the payment in the main currency
     * @var float
     * @SWG\Property(format="double")
     * @Column(type="float")
     */
    protected $amount;
    public function getAmount() { return round($this->amount, 5); }
    public function setAmount($amount) {
        $this->amount = round($amount, 5);
    }

    /**
     * Id of the Currency of the Payment
     * @var int
     * @SWG\Property(format="int32")
     * @ManyToOne(targetEntity="\Pasteque\Server\Model\Currency")
     * @JoinColumn(name="currency_id", referencedColumnName="id", nullable=false)
     */
    protected $currency;
    public function getCurrency() { return $this->currency; }
    public function setCurrency($currency) { $this->currency = $currency; }

    /**
     * Amount of the Payment in the used Currency
     * @var float
     * @SWG\Property(format="double")
     * @Column(type="float")
     */
    public $currencyAmount;
    public function getCurrencyAmount() {
        return round($this->currencyAmount, 5);
    }
    public function setCurrencyAmount($currencyAmount) {
        $this->currencyAmount = round($currencyAmount, 5);
    }

    public static function fromStruct($struct, $dao, $parent = null) {
        // This is the old desktop compatibility mode
        // which doesn't use PaymentMode.
        if (!empty($struct['desktop'])) {
            $payment = new TicketPayment();
            $payment->setDispOrder($struct['dispOrder']);
            $payment->setAmount($struct['amount']);
            $payment->setCurrencyAmount($struct['currencyAmount']);
            $currencyField = static::getAssociationFields()[2];
            $payment->setCurrency($payment->readAssociationValue($struct, $dao, $currencyField));
            $search = $dao->search(\Pasteque\Server\Model\PaymentMode::class,
                    new DAOCondition('reference', '=', $struct['type']));
            if (count($search) > 0) {
                $payment->setPaymentMode($search[0]);
            } else {
                throw new \UnexpectedValueException(sprintf('PaymentMode %s was not found', $struct['type']));
            }
            return $payment;
        }
        // And this is the normal case
        return parent::fromStruct($struct, $dao, $parent);
    }

    public function toStruct() {
        $struct = parent::toStruct();
        // Add 'type' for compatibility with Desktop
        // which doesn't use PaymentMode.
        $struct['type'] = $this->getPaymentMode()->getReference();
        return $struct;
    }

}
