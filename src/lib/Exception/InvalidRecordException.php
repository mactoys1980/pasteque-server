<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Server\Exception;

/**
 * Writing a record failed because it didn't satisfy model-wide constraints.
 */
class InvalidRecordException extends \Exception implements PastequeException
{
    /** The record is read only. */
    const CSTR_READ_ONLY = 'read_only_record';
    /** The record is generated and cannot be written directly. */
    const CSTR_GENERATED = 'generated_record';
    /** The requested record to update was not found at all. */
    const CSTR_MAIN_RECORD_NOT_FOUND = 'main_record_not_found';

    private $constraint;
    private $class;
    private $id;

    public function __construct($constraint, $class, $id) {
        $this->constraint = $constraint;
        $this->class = $class;
        $this->id = $id;
        switch ($constraint) {
            case static::CSTR_READ_ONLY:
                $msg = sprintf('%s is read only (tried to update id %s).',
                        $this->class, json_encode($this->getJsonableId()));
                break;
            case static::CSTR_GENERATED:
                $msg = sprintf('%s id %s is generated and cannot be written directly.',
                        $this->class, json_encode($this->getJsonableId()));
                break;
            case static::CSTR_MAIN_RECORD_NOT_FOUND:
                $msg = sprintf('%s id %s was not found.',
                        $this->class, json_encode($this->getJsonableId()));
                break;
            default:
                $msg = sprintf('Invalid record error on %s id %s.',
                        $this->class, json_encode($this->getJsonableId()));
                break;
        }
        parent::__construct($msg);
    }

    public function getConstraint() {
        return $this->constraint;
    }

    public function getClass() {
        return $this->class;
    }

    public function getId() {
        return $this->id;
    }

    public function getJsonableId() {
        if (gettype($this->id) == 'resource') {
            return '<resource>';
        } else {
            return $this->id;
        }
    }

    public function toStruct() {
        return [
            'error' => 'InvalidRecordException',
            'constraint' => $this->constraint,
            'class' => $this->class,
            'id' => $this->getJsonableId(),
            'message' => $this->message,
        ];
    }
}
