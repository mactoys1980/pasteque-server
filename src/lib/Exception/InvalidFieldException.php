<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Server\Exception;

/**
 * Writing a record failed because one of it's fields has an invalid value.
 */
class InvalidFieldException extends \Exception implements PastequeException
{
    /** The field is read only. */
    const CSTR_READ_ONLY = 'read_only_field';
    /** The value must be unique. */
    const CSTR_UNIQUE = 'unique_value';
    /** The field is a reference to an other record but this record
     * was not found. */
    const CSTR_ASSOCIATION_NOT_FOUND = 'association_not_found';
    /** The field is null but it cannot be. */
    const CSTR_NOT_NULL = 'not_null';
    /** The field is a reference to a cash session but this session
     * must be opened. */
    const CSTR_OPENED_CASH = 'opened_cash_required';

    private $constraint;
    private $class;
    private $field;
    private $id;
    private $value;

    public function __construct($constraint, $class, $field, $id, $value) {
        $this->constraint = $constraint;
        $this->class = $class;
        $this->field = $field;
        $this->id = $id;
        $this->value = $value;
        switch ($constraint) {
            case static::CSTR_READ_ONLY:
                $msg = sprintf('The field %s of %s is read only (write to update %s with value %s).',
                        $this->field, $this->class,
                        json_encode($this->getJsonableId()),
                        json_encode($this->getJsonableValue()));
                break;
            case static::CSTR_UNIQUE:
                $msg = sprintf('The field %s of %s must be unique (write to update %s with value %s).',
                        $this->field, $this->class,
                        json_encode($this->getJsonableId()),
                        json_encode($this->getJsonableValue()));
                break;
            case static::CSTR_OPENED_CASH:
                $msg = sprintf('The associated associated cash session in %s of %s must be opened (tried to write %s with value %s).',
                        $this->field, $this->class,
                        json_encode($this->getJsonableid()),
                        json_encode($this->getJsonableValue()));
                break;
        }
        parent::__construct($msg);
    }

    public function getConstraint() {
        return $this->constraint;
    }

    public function getClass() {
        return $this->class;
    }

    public function getField() {
        return $this->field;
    }

    public function getId() {
        return $this->id;
    }

    public function getValue() {
        return $this->value;
    }

    public function getJsonableId() {
        if (gettype($this->id) == 'resource') {
            return '<resource>';
        } else {
            return $this->id;
        }
    }

    public function getJsonableValue() {
        if (gettype($this->value) == 'resource') {
            return '<resource>';
        } else {
            return $this->value;
        }
    }

    public function toStruct() {
        return [
            'error' => 'InvalidFieldException',
            'constraint' => $this->constraint,
            'class' => $this->class,
            'field' => $this->field,
            'id' => $this->getJsonableId(),
            'value' => $this->getJsonableValue(),
            'message' => $this->message,
        ];
    }
}
