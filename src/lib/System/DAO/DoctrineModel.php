<?php
//    Pastèque API
//
//    Copyright (C) 2012-2015 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Server\System\DAO;

use \Pasteque\Server\Exception\RecordNotFoundException;

/** Parent class of all models to use with Doctrine. */
abstract class DoctrineModel
{
    /** List primitive typed field names of the model in an array, excluding id. */
    protected abstract static function getDirectFieldNames();
    /** List reference field of the model in an array. A field is an associative
     * array with the following keys:
     * name: The field name (declared in code)
     * class: The full class name of the reference.
     * array (optional): can only be true if set, flag for XtoMany fields.
     * null (optional): can only be true if set, flag for nullable.
     * embedded (optional): can only be true if set, flag for subclasses.
     * Embedded values can be created on the fly from struct and are embedded
     * in toStruct. Non embedded fields are referenced only by id.
     * Embedded classes don't have their own id.
     * internal (optional): can only be true if set. Internal fields are not
     * read and exported in structs.
     * */
    protected abstract static function getAssociationFields();

    // Required for toStruct for references. */
    public function getId() { return $this->id; }

    public function idEquals($otherModel) {
        $myId = $this->getId();
        $otherId = $otherModel->getId();
        if (!is_array($myId) && !is_array($otherId)) {
            return $myId === $otherId;
        } else if (is_array($myId) && is_array($otherId)) {
            foreach ($myId as $key => $value) {
                if (!isset($otherId[$key]) || $value !== $otherId[$key]) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    protected function directFieldToStruct($field) {
        $value = call_user_func(array($this, 'get' . ucfirst($field)));
        switch (gettype($value)) {
            case 'resource':
                return base64_encode(stream_get_contents($value));
            default:
                return $value;
        }
    }

    protected function associationFieldToStruct($field) {
        $value = call_user_func(array($this, 'get' . ucfirst($field['name'])));
        // Association field
        if (!empty($field['array'])) {
            // Value is a ArrayCollection
            $struct = array();
            foreach ($value as $v) {
                if (empty($field['embedded'])) {
                    $struct[] = $v->getId();
                } else {
                    $struct[] = $v->toStruct();
                }
            }
            return $struct;
        } else {
            if ($value === null) {
                return null;
            } else {
                if (empty($field['embedded'])) {
                    return $value->getId();
                } else {
                    return $value->toStruct();
                }
            }
        }
    }

    /** Unlink the model from DAO and all methods.
     * All references are converted to their Id.
     * WARNING: Use to/fromStruct only once and do never switch back and forth
     * from struct to DoctrineModel, as it may makes Doctrine mess up with the
     * ids and duplicate or delete some records.
     * @return An associative array with raw data, suitable for
     * json encoding. */
    public function toStruct() {
        // Get Doctrine fields and render them to delete the proxies
        $data = ['id' => $this->getId()];
        foreach (static::getDirectFieldNames() as $field) {
            $data[$field] = $this->directFieldToStruct($field);
        }
        foreach (static::getAssociationFields() as $field) {
            if (empty($field['internal'])) {
                $data[$field['name']] = $this->associationFieldToStruct($field);
            }
        }
        return $data;
    }

    /** Find the name of the method to operate the given field.
     * @param $operation The name of the operation to execute ('set', 'add' etc)
     * @param $fieldName The name of the field.
     * @return The name of the function. Null if not found. */
    protected function findMethodName($operation, $fieldName) {
        $methodName = $operation . ucfirst($fieldName);
        if (is_callable([$this, $methodName])) {
            return $methodName;
        }
        // Maybe field is plural (i.e. addLine for lines)
        $methodName = substr($methodName, 0, -1);
        if (is_callable([$this, $methodName])) {
            return $methodName;
        }
        // Maybe field is plural with 'e' (i.e. addTax for taxes)
        $methodName = substr($methodName, 0, -1);
        if (is_callable([$this, $methodName])) {
            return $methodName;
        }
        return null;
    }
    /** Call findMethodName and throw a ReflectionException if no method is found. */
    protected function findMethodNameOrThrow($operation, $fieldName) {
        $methodName = $this->findMethodName($operation, $fieldName);
        if ($methodName == null) {
            throw new \ReflectionException(sprintf('Method to %s %s on %s was not found', $operation, $fieldName, static::class));
        }
        return $methodName;
    }

    /*
     * Build an instance from struct. If id is set, it will ask the DAO to set
     * the current values. If not a new instance is created and returned.
     * @param $struct The record as associative array.
     * @param $dao The DAO to use.
     * @throws RecordNotFoundException If id is set but no data is found,
     * or if an associated record was not found with the given id.
     */
    protected static function instanceFromStruct($struct, $dao,
            $parent = null) {
        // $parent is only for recursive internal calls.
        $model = null;
        if (isset($struct['id'])) {
            $model = $dao->read(static::class, $struct['id']);
            if ($model === null && $parent === null) {
                // Embedded classes don't have their own id, so it's ok
                // not to find them: it's a new instance.
                throw new RecordNotFoundException(static::class, $struct['id']);
            }
        } else {
            /* Maybe the id was not set, because that doesn't make sense
             * to do it for an embedded data. */
            if ($parent !== null && $parent->getId() != null) {
                /* Try to rebuild the id automatically and look for the record
                 * when updating an existing parent record.
                 * This is necessary because Doctrine will fail to write it
                 * because of the unbound already existing record.
                 * Unity constraint violation exception is raised in that case. */
                $autoId = [];
                $idFields = $dao->getEntityManager()->getClassMetadata(static::class)->getIdentifier();
                if (count($idFields) == 1 && $idFields[0] == 'id') {
                    // Stand-alone embedded record
                    $model = new static();
                } else {
                    foreach ($idFields as $idField) {
                        // Look for a direct field, that is surely set.
                        foreach (static::getDirectFieldNames() as $field) {
                            if ($idField == $field) {
                                $autoId[$field] = $struct[$field];
                                break;
                            }
                        }
                        if (!empty($autoId[$idField])) {
                            // Found in direct fields.
                            continue;
                        }
                        /* Look for an associative field.
                         * It can be the parent (embedded)
                         * or an other inner field. */
                        foreach (static::getAssociationFields() as $field) {
                            if ($idField == $field['name']) {
                                if ($field['class'] == "\\" . get_class($parent)) {
                                    $autoId[$field['name']] = $parent->getId();
                                    break;
                                } elseif (array_key_exists($field['name'], $struct)) {
                                    $autoId[$field['name']] = $struct[$field['name']];
                                }
                            }
                        }
                    }
                }
                // Now that the Id is recovered, try to read it.
                if ($model === null) {
                    $model = $dao->read(static::class, $autoId);
                }
            }
            // If nothing was found, it is a new record.
            if ($model === null) {
                $model = new static();
            }
        }
        return $model;
    }

    /** Set the primitive typed fields from struct. */
    protected function directFieldsFromStruct($struct, $dao) {
        foreach (static::getDirectFieldNames() as $field) {
            if ($field == 'id' || $field == 'hasImage') {
                // Doctrine loads the id
                // and image cannot be alterated outside imageAPI
                continue;
            }
            if (array_key_exists($field, $struct)) {
                $value = $struct[$field];
                $setter = $this->findMethodNameOrThrow('set', $field);
                call_user_func([$this, $setter], $struct[$field]);
            }
        }
    }

    /**
     * Get an associated record to link it to it's parent.
     * @param $struct The parent data as associative array.
     * @param $dao The DAO to use.
     * @param $field The field description of the association (from
     * getAssociationFields).
     * @return The associated record or an array of associated records.
     * @throws RecordNotFoundException when an associated record cannot be
     * found with the given id (only for non-embedded records).
     */
    protected function readAssociationValue($struct, $dao, $field) {
        if (!empty($field['array'])) {
            $submodels = new \Doctrine\Common\Collections\ArrayCollection();
            foreach ($struct[$field['name']] as $content) {
                if (!empty($field['embedded'])) {
                    $fromStruct = new \ReflectionMethod($field['class'], 'fromStruct');
                    $submodel = $fromStruct->invoke(null, $content, $dao, $this);
                } else {
                    $submodel = $dao->read($field['class'], $content['id']);
                    if ($submodel === null) {
                        throw new RecordNotFoundException($field['class'],
                                $struct[$field['name']]);
                    }
                }
                $submodels->add($submodel);
            }
            return $submodels;
        } else {
            if (!empty($field['embedded'])) {
                $fromStruct = new \ReflectionMethod($field['class'], 'fromStruct');
                $structId = null;
                $submodel = $fromStruct->invoke(null, $struct[$field['name']], $dao, $this);
            } else {
                $submodel = $dao->read($field['class'], $struct[$field['name']]);
                if ($submodel === null) {
                    throw new RecordNotFoundException($field['class'],
                            $struct[$field['name']]);
                }
            }
            return $submodel;
        }
    }

    /**
     * Build a model linked to the DAO from raw data.
     * @param $struct An associative array containing the data
     * (hasImage is a special field and is ignored from the struct).
     * WARNING: Use to/fromStruct only once and do never switch back and forth
     * from struct to DoctrineModel, as it may makes Doctrine mess up with the
     * ids and duplicate or delete some records.
     * @param $dao The DAO to link to.
     * @param $parent Only for recursive calls, don't set it.
     * @throws RecordNotFoundException If Id is set but not data is found,
     * either for this record or one of it's associated records.
     * @throws InvalidFieldException When an non nullable association field
     * is not set (NOT_NULL).
     */
    public static function fromStruct($struct, $dao, $parent = null) {
        $model = static::instanceFromStruct($struct, $dao, $parent);
        $model->directFieldsFromStruct($struct, $dao);
        $associationFields = static::getAssociationFields();
        foreach ($associationFields as $field) {
            if (!empty($field['internal'])) {
                // Internal fields are read from dao in instanceFromStruct
                continue;
            }
            $fieldName = $field['name'];
            // Check for required (not null)
            if (empty($field['null'])
                    && (!array_key_exists($fieldName, $struct) || $struct[$fieldName] === null)) {
                if (empty($field['array'])) {
                    throw new InvalidFieldException(InvalidFieldException::NOT_NULL,
                            static::class, $field['name'],
                            $struct['id'], null);
                } else {
                    // Allow omitting an array for keeping its value
                    // or setting it to null to clear it.
                }
            }
            // Get value and assign it.
            if (array_key_exists($fieldName, $struct) // is set
                    && $struct[$fieldName] !== null   // and not null
                    && (!is_array($struct[$fieldName]) || count($struct[$fieldName]) >= 0)) { // and not an empty array
                $value = $model->readAssociationValue($struct, $dao, $field);
                if (!empty($field['array'])) {
                    /* At that point the model is filled from database by Doctrine.
                     * We must remove records that are not listed in struct
                     * and add those that are new.
                     * Unbind/rebind doesn't work because it makes Doctrine ignore
                     * the id. */
                    $toAdd = [];
                    $toRemove = [];
                    // Look for records to remove
                    $getter = $model->findMethodNameOrThrow('get', $fieldName);
                    $dbValues = call_user_func([$model, $getter]);
                    foreach ($dbValues as $dbv) {
                        $found = false;
                        foreach ($value as $v) {
                            if ($dbv->idEquals($v)) {
                                $found = true;
                                continue;
                            }
                        }
                        if (!$found) {
                            $toRemove[] = $dbv;
                        }
                    }
                    // Look for records to add
                    foreach ($value as $v) {
                        $id = $v->getId();
                        if ($id == null) { // New record, add it.
                            $toAdd[] = $v;
                            continue;
                        } else {
                            // For non-incremental id, check if the record
                            // is already loaded or not. Add it if new.
                            $found = false;
                            foreach ($dbValues as $dbv) {
                                if ($dbv->idEquals($v)) {
                                    $found = true;
                                    break;
                                }
                            }
                            if (!$found) {
                                $toAdd[] = $v;
                            }
                        }
                    }
                    // Add and remove
                    $remover = $model->findMethodNameOrThrow('remove', $fieldName);
                    foreach ($toRemove as $rm) {
                        call_user_func([$model, $remover], $rm);
                    }
                    $adder = $model->findMethodNameOrThrow('add', $fieldName);
                    foreach ($toAdd as $add) {
                        call_user_func([$model, $adder], $add);
                    }
                } else {
                    $setter = $model->findMethodNameOrThrow('set', $fieldName);
                    call_user_func([$model, $setter], $value);
                }
            } else if (array_key_exists($fieldName, $struct)) {
                // either null or an empty array
                $setter = $model->findMethodNameOrThrow('set', $fieldName);
                // Delete association on explicit null value
                if (empty($field['array'])) {
                    // Erase association for empty values
                    call_user_func([$model, $setter], null);
                } else {
                    // Clear array for arrays
                    call_user_func([$model, $setter], []);
                }
            } // Not set: keep the current association value which was set by Doctrine
        }
        return $model;
    }

}
