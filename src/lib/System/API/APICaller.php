<?php
//    Pastèque Web back office
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace Pasteque\Server\System\API;

use \Pasteque\Server\Exception\APINotFoundException;
use \Pasteque\Server\Exception\InvalidFieldException;
use \Pasteque\Server\Exception\InvalidRecordException;
use \Pasteque\Server\System\Login;

/** Utility class to call API methods. */
class APICaller {

    public static function formatAPIName($apiName) {
        $name = strtolower($apiName);
        $name = str_replace('..', '', $name);
        if (substr($name, -3) == 'api') {
            $name = substr($name, 0, -3);
        }
        $name = ucfirst($name);
        return sprintf('%sAPI', $name);
    }

    /**
     * Run an API method and wrap the response in an APIResult.
     */
    public static function run($app, $apiName, $methodName, $args = array()) {
        if ($args === null) { $args = array(); }
        if (!is_array($args)) { $args = array($args); }
        // Get API class
        $apiName = APICaller::formatAPIName($apiName);
        $apiPkg = $app->isFiscalMirror() && strtolower($apiName) !== 'login' ?
                 'FiscalMirrorAPI' : 'API'; // TODO: ugly exception
        $className = sprintf('\Pasteque\Server\%s\%s', $apiPkg, $apiName);
        if (class_exists($className, true)) {
            // Class found, get requested method
            $apiClass = new \ReflectionClass($className);
            if (!$apiClass->implementsInterface('\Pasteque\Server\API\API')) { 
                return APIResult::reject(new APINotFoundException($apiName));
            }
            $constructor = $apiClass->getMethod('fromApp');
            $api = $constructor->invoke(null, $app);
            if (method_exists($api, $methodName)) {
                // Method found, check arguments
                $method = new \ReflectionMethod($api, $methodName);
                $methodParser = new APIMethodParser($api, $method);
                if (!$methodParser->checkArgc($args)) {
                    return APIResult::reject(new APINotFoundException($apiName,
                            $methodName,
                            $methodParser->getMinArgc(),
                            $methodParser->getMaxArgc(),
                            count($args)));
                }
                try {
                    $realArgs = $methodParser->buildArgsArray($args);
                    return APIResult::success($method->invokeArgs($api,
                            $realArgs));
                } catch (InvalidRecordException $e) {
                    return APIResult::reject($e);
                } catch (InvalidFieldException $e) {
                    return APIResult::reject($e);
                } catch (\BadMethodCallException $e) {
                    return APIResult::reject($e->getMessage());
                } catch (\UnexpectedValueException $e) {
                    return APIResult::reject($e->getMessage());
                } catch (\ReflectionException $e) {
                    $app->getLogger()->error('Internal error while calling API',
                            array('exception' => $e));
                    return APIResult::error($e->__toString());
                } catch (\Exception $e) {
                    $app->getLogger()->error('Internal error',
                            array('exception' => $e));
                    return APIResult::error($e->__toString());
                }
            } else {
                return APIResult::reject(new APINotFoundException($apiName,
                        $methodName));
            }
        } // end class exists
        return APIResult::reject(new APINotFoundException($apiName));
    }

    /** Check if the user is authenticated and has permission.
     * @param $userId The user requesting the call.
     * @param $apiName Targeted API name
     * @param $method Targeted API method. */
    public static function checkPermission($userId, $apiName, $method) {
        $apiName = APICaller::formatAPIName($apiName);
        if ($userId == null) {
            // Accept only Login->login for unauthenticated users
            return ($apiName == 'LoginAPI'
                && ($method == 'getToken' || $method == 'login'));
        } else {
            // This is where permission checking will be done
            return true;
        }
    }

    public static function isAllowedOrigin($origin, $allowedOrigins) {
        // Accept *, single value match or in array
        return ($allowedOrigins === '*'
                || (!is_array($allowedOrigins) && $origin === $allowedOrigins)
                || (is_array($allowedOrigins) && in_array($origin, $allowedOrigins)));
    }

    /** Get HTTP headers to allow a cross origin request. */
    public static function getCORSHeaders($origin, $allowedOrigins, $maxAge = 86400) {
        $headers = [];
        if (!static::isAllowedOrigin($origin, $allowedOrigins)) {
            return $headers;
        }
        $headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, DELETE, OPTIONS';
        $headers['Access-Control-Allow-Origin'] = $origin;
        $headers['Access-Control-Allow-Credentials'] = false;
        $headers['Access-Control-Max-Age'] = $maxAge;
        $headers['Access-Control-Expose-Headers'] = Login::TOKEN_HEADER;
        $headers['Access-Control-Allow-Headers'] = Login::TOKEN_HEADER . ', Content-Type';
        if (is_array($allowedOrigins)) { $headers['Vary'] = 'Origin'; }
        return $headers;
    }

}
