CREATE TABLE fiscaltickets (type VARCHAR(255) NOT NULL, sequence VARCHAR(255) NOT NULL, number INT NOT NULL, date DATETIME NOT NULL, content LONGTEXT NOT NULL, signature VARCHAR(255) NOT NULL, PRIMARY KEY(type, sequence, number)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

