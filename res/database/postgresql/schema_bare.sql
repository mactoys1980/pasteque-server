CREATE TABLE fiscaltickets (type VARCHAR(255) NOT NULL, sequence VARCHAR(255) NOT NULL, number INT NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, content TEXT NOT NULL, signature VARCHAR(255) NOT NULL, PRIMARY KEY(type, sequence, number));

