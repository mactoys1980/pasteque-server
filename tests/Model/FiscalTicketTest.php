<?php
//    Pasteque server testing
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pasteque.
//
//    Pasteque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pasteque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pasteque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque\Server;

use \Pasteque\Server\Model\FiscalTicket;
use \PHPUnit\Framework\TestCase;

require_once(dirname(dirname(__FILE__)) . "/common_load.php");

class FiscalTicketTest extends TestCase
{
    protected function setUp(): void {
    }

    protected function tearDown(): void {
    }

    public function testUpdateSignature() {
        $this->expectException(\InvalidArgumentException::class);
        $fiscalTicket = new FiscalTicket();
        $fiscalTicket->setSequence('00001');
        $fiscalTicket->setNumber(1);
        $fiscalTicket->setDate(new \DateTime());
        $fiscalTicket->setContent('I am a ticket');
        $fiscalTicket->setSignature('I am a poor lonesome signature');
        $this->assertTrue(true, 'Failing too soon');
        $fiscalTicket->setSignature('I am an evil sigature, niark niark');
    }

    public function testUpdateEOSignature() {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Changing the signature is not allowed.');
        $fiscalTicket = new FiscalTicket();
        $fiscalTicket->setSequence('00001');
        $fiscalTicket->setNumber(null);
        $fiscalTicket->setDate(new \DateTime());
        $fiscalTicket->setContent('EOS');
        $fiscalTicket->setSignature('I am a poor lonesome signature');
        $fiscalTicket->setSignature('Oh wait!');
    }

    public function testSignatureFirst() {
        $fiscalTicket = new FiscalTicket();
        $fiscalTicket->setSequence('00001');
        $fiscalTicket->setNumber(1);
        $fiscalTicket->setDate(new \DateTime());
        $fiscalTicket->setContent('I am a ticket');
        $fiscalTicket->sign(null);
        $falseTicket = new FiscalTicket();
        $falseTicket->setSequence('00001');
        $falseTicket->setNumber(0);
        $falseTicket->setDate(new \DateTime());
        $falseTicket->setContent('I am a ticket');
        $falseTicket->sign(null);
        $this->assertTrue($fiscalTicket->checkSignature(null));
        $this->assertFalse($fiscalTicket->checkSignature($falseTicket));
    }

    /** @depends testSignatureFirst */
    public function testSignatureChained() {
        $fiscalTicket = new FiscalTicket();
        $fiscalTicket->setSequence('00001');
        $fiscalTicket->setNumber(1);
        $fiscalTicket->setDate(new \DateTime());
        $fiscalTicket->setContent('I am a ticket');
        $fiscalTicket->sign(null);
        $fiscalTicket2 = new FiscalTicket();
        $fiscalTicket2->setSequence('00001');
        $fiscalTicket2->setNumber(2);
        $fiscalTicket2->setDate(new \DateTime());
        $fiscalTicket2->setContent('I will always be the second.');
        $fiscalTicket2->sign($fiscalTicket);
        $this->assertTrue($fiscalTicket->checkSignature(null));
        $this->assertTrue($fiscalTicket2->checkSignature($fiscalTicket));
        $this->assertFalse($fiscalTicket2->checkSignature(null));
    }

}
