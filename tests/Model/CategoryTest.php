<?php
//    Pasteque server testing
//
//    Copyright (C) 
//			2012 Scil (http://scil.coop)
//			2017 Karamel, Association Pastèque (karamel@creativekara.fr, https://pasteque.org)
//
//    This file is part of Pasteque.
//
//    Pasteque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pasteque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pasteque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque\Server;

use \Pasteque\Server\Model\Category;
use \Pasteque\Server\System\DAO\DAOCondition;
use \Pasteque\Server\System\DAO\DAOFactory;
use \PHPUnit\Framework\TestCase;

require_once(dirname(dirname(__FILE__)) . "/common_load.php");

/** Test DoctrineModel functions through Category */
class CategoryTest extends TestCase
{
    private $dao;

    protected function setUp(): void {
        global $dbInfo;
        $this->dao = DAOFactory::getDAO($dbInfo, ['debug' => true]);
    }

    protected function tearDown(): void {
        // Explicitely delete children before parents because Doctrine
        // cannot handle it. It won't work if there grandchildren.
        $children = $this->dao->search(Category::class,
                new DAOCondition('parent', '!=', null));
        foreach ($children as $record) {
            $this->dao->delete($record);
        }
        $parents = $this->dao->search(Category::class,
                new DAOCondition('parent', '=', null));
        foreach ($parents as $record) {
            $this->dao->delete($record);
        }
        $this->dao->commit();
        $this->dao->close();
    }

    public function testToStructNoRef() {
        $cat = new Category();
        $cat->setReference('Ref');
        $cat->setLabel('label');
        $struct = $cat->toStruct();
        $this->assertNull($cat->getId());
        $this->assertNull($struct['id']);
        $this->assertEquals($cat->getReference(), $struct['reference']);
        $this->assertEquals($cat->getLabel(), $struct['label']);
        $this->assertNull($cat->getParent());
        $this->assertNull($struct['parent']);
        $this->assertEquals($cat->hasImage(), $struct['hasImage']);
        $this->assertEquals($cat->getDispOrder(), $struct['dispOrder']);
    }

    /** @depends testToStructNoRef */
    public function testToStructRef() {
        $cat = new Category();
        $cat->setReference('Ref');
        $cat->setLabel('label');
        $childCat = new Category();
        $childCat->setReference('Child');
        $childCat->setLabel('child label');
        $childCat->setParent($cat);
        $this->dao->write($cat);
        $this->dao->write($childCat);
        $this->dao->commit();
        $struct = $childCat->toStruct();
        $this->assertEquals($cat->getId(), $struct['parent']);
    }

    public function testFromStructNoRef() {
        $default = new Category();
        $struct = array('reference' => 'Ref', 'label' => 'lbl');
        $cat = Category::fromStruct($struct, $this->dao);
        $this->assertNull($cat->getId());
        $this->assertEquals($struct['reference'], $cat->getReference());
        $this->assertEquals($struct['label'], $cat->getLabel());
        $this->assertNull($cat->getParent());
        $this->assertFalse($cat->hasImage());
        $this->assertEquals($default->getDispOrder(), $cat->getDispOrder());
    }

    /** @depends testFromStructNoRef */
    public function testFromStructRefError() {
        $this->expectException(\UnexpectedValueException::class);
        $struct = array('reference' => 'Ref', 'label' => 'lbl', 'parent' => 10);
        $cat = Category::fromStruct($struct, $this->dao);
    }

    /** @depends testFromStructNoRef */
    public function testFromStructRef() {
        $cat = new Category();
        $cat->setReference('Ref');
        $cat->setLabel('label');
        $this->dao->write($cat);
        $this->dao->commit();
        $id = $cat->getId();
        $struct = array('reference' => 'Child', 'label' => 'child', 'parent' => $id);
        $child = Category::fromStruct($struct, $this->dao);
        $parent = $child->getParent();
        $this->assertNotNull($parent);
        $this->assertEquals($id, $parent->getId());
        $this->assertEquals($cat->getReference(), $parent->getReference());
        $this->assertEquals($cat->getLabel(), $parent->getLabel());
        $this->assertEquals($cat->getDispOrder(), $parent->getDispOrder());
    }

    /** Check if the model created is linked to Doctrine
     * @depends testFromStructNoRef */
    public function testFromStructId() {
        $cat = new Category();
        $cat->setReference('Ref');
        $cat->setLabel('label');
        $this->dao->write($cat);
        $this->dao->commit();
        $id = $cat->getId();
        $struct = array('id' => $id, 'reference' => 'Edit', 'label' => 'edited');
        $editCat = Category::fromStruct($struct, $this->dao);
        $this->dao->write($editCat);
        $this->dao->commit();
        $this->assertEquals($cat->getId(), $editCat->getId());
        $read = $this->dao->read(Category::class, $id);
        $this->assertEquals('Edit', $read->getReference());
        $this->assertEquals('edited', $read->getLabel());
    }
}
